function computeWeightPounds() {
    var pounds = parseFloat(document.getElementById('pound').value);

    let kilos = pounds / 2.205 ;
    kilos = kilos.toFixed(1);
    // made by dab
    document.getElementById('result0').innerHTML = "Given weight in Kilograms is " + kilos ;
}

function computeWeightKilograms() {
    var kilograms = parseFloat(document.getElementById('kilogram').value);

    let poundss = kilograms * 2.205 ;
    poundss = poundss.toFixed(1);
    // made by dab
    document.getElementById('result1').innerHTML = "Given weight in Pounds is " + poundss ;
}